## Backend Test Project

This test is divided into *FOUR* mandatory challenge and 2 optional challenges.

Since our stacks are composed of PHP (Laravel & MySQL) and Node.js (Express & MongoDB), we require you to work on this project with PHP and MySQL **OR** Javascript and MongoDB.

You can however use ANY framework you like. Eg: Symfony for PHP or Koa for Node.js

In order to submit the project, you MUST complete **Challenge 1** & **Challenge 2**.
For every optional challenge completed, you will earn score that will be summed into your final score.

We will select candidates who gets the highest score
_(You do not need to complete all)_

### Submission
Submission can be done in TWO ways:

1. Send zipped project files to yohanes@irvinsaltedegg.com
2. **EXTRA 50 POINT**: Upload it to Github and email Github link to yohanes@irvinsaltedegg.com

---

## Challenge 1 (MANDATORY)
**Create this endpoint: [POST] `/api/product`**
Validate and save a Product into database if successful. Product should have these properties:
- id
- name (mandatory)
- price (mandatory)

If successful, this endpoint must return a JSON response and `201` status code.
```
{
  "data": {
    "_id": "507f191e810c19729de860ea",
    "name": "Product 1",
    "price": 100000
  }
}
```

## Challenge 2 (MANDATORY)
**Create this endpoint: [GET] `/api/product`**
Get product detail by ID from database
If successful, this endpoint must return a JSON response of updated product and `200` status code.
```
{
  "data": {
    "_id": "507f191e810c19729de860ea",
    "name": "Product 1",
    "price": 100000
  }
}
```

## Challenge 3 (MANDATORY)
**Create this endpoint: [PUT] `/api/product/{id}`**
Validate and update a Product.
If successful, this endpoint must return a JSON response of deleted product id and `200` status code.
```
{
  "data": {
    "_id": "507f191e810c19729de860ea",
    "name": "Product 1",
    "price": 100000
  }
}
```

## Challenge 4 (MANDATORY)
**Create this endpoint: [DELETE] `/api/product/{id}`**
Delete a Product.
If successful, this endpoint must return a JSON response of deleted product id and `200` status code.
```
{
  "data": [
  	"507f191e810c19729de860ea"
  ]
}
```

---

### OPTIONAL CHALLENGES

### Challenge 5 (Max score: 100)
Create unit-tests for each endpoint

### Challenge 6 (Max score: 50)
Submit your project on Github
