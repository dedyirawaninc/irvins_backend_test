<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        // @@@ product table seeder

        $faker = \Faker\Factory::create();

        // adding additional formatter
        $faker->addProvider(new \Bezhanov\Faker\Provider\Commerce($faker));
 
        // creating of 50 samples records
        for ($i = 0; $i < 50; $i++) {
            Product::create([
                '_id' => $faker->sha1,
                'name' => $faker->productName,
                'price' => $faker->randomNumber(2)
            ]);
        }
    }
}
