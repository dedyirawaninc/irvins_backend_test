<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

// @@@ product unit test

class ProductTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testCreateProduct()
    {
        $data = [
            '_id' => '4ab3d15d7ab2ee74093f4b892a078e71bff0e6bf',
            'name' => "New Product 1",
            'price' => 199,
        ];

        $response = $this->json('POST', '/api/products', $data);
        $response->assertStatus(200);
    }

    public function testDeleteProduct()
    {
        $response = $this->json('GET', '/api/products');
        $response->assertStatus(200);

        $product = $response->getData()[0];

        $delete = $this->json('DELETE', '/api/products/'.$product->_id);
        $delete->assertStatus(200);
    }

    public function testGetAllProducts()
    {
        $response = $this->json('GET', '/api/products');
        $response->assertStatus(200);
    }

    public function testGetOneProduct()
    {
        $response = $this->json('GET', '/api/products');
        $response->assertStatus(200);

        $product = $response->getData()[0];

        $delete = $this->json('GET', '/api/products/'.$product->_id);
        $delete->assertStatus(200);
    }
}
