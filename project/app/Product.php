<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// @@@ product model

class Product extends Model
{
    // primary keys
    protected $primaryKey = '_id';
    public $incrementing = false;
    public $timestamps = true;

    // adding fillable properties
    protected $fillable = ['_id', 'name', 'price'];
}
